<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="obrigado">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-lg-8">
						<div class="row cabecalho justify-content-center">
							
							<div class="col-12 col-lg-10">
								<h2>Muito obrigado!</h2>

								<h3>Em breve você receberá o e-book<br> em seu e-mail cadastrado.</h3>

								<span>Nossa equipe preparou esse book para ajudar em seu esclarecimento de questões sobre os diversos assuntos da medicina. Ainda em caso de dúvidas nos contate por meio do whatsapp, no botão disponível logo abaixo.</span>

								<a target="_blank" href="https://wa.me/5573998194979">
									<span class="wa">Acesse o Whatsapp da Zaganelli Oliveira. <img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeWhatsapp.png"></span> 
								</a>



								<span>Gostaria de agendar sua consulta em uma das<br> especialidades oferecidas pela Zaganelli Oliveira?</span>
								
								<a href="<?php echo home_url('agendamento'); ?>">
									<button>Agende sua consulta aqui</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();