<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zaganelli_Oliveira
 */

?>

	</main><!-- #main -->

	<footer id="colophon" class="site-footer">
		
		<?php if(!is_page('agendamento')): ?>
		<a href="<?php echo home_url('agendamento'); ?>">
			<img class="btAgendar" src="<?php echo get_template_directory_uri(); ?>/assets/img/btAgenda.png" />
		</a>
		<?php endif; ?>

		<div class="btRedes">
			<a href="tel:557332921717" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/btTelefone.png" />
			</a>
			<a href="https://www.instagram.com/zaganelli.oliveira/" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/btInstagram.png" />
			</a>
			<a href="https://www.facebook.com/zaganellioliveira/" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/btFacebook.png" />
			</a>
		</div>

		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/upperpart.png" class="upperpart" />
		<div class="container site-info">
			<div class="row justify-content-center">
				<div class="col-12 col-md-8">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" class="logo" />
					</a>
					<div class="endereco">
						<a href="https://goo.gl/maps/w66ai6gH7PSC98VG7">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icPI.png" />
							<span>Av. Getúlio Vargas 2879  • Bela vista  •  Teixeira de Freitas-BA  •  Cep 45990-289</span>
						</a>
						<br>
						<a href="tel:557332921717">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icTF.png" />
							<span>Tel (73) 3292-1717</span>
						</a>
						<span>&nbsp;&nbsp;•&nbsp;&nbsp;</span>
						<a href="https://wa.me/5573998194979">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icWA.png" />
							<span>(73) 99819-4979</span>
						</a>
						<br>
						<a href="mailto:recepcao@zaganellioliveira.com.br">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icEM.png" />
							<span>recepcao@zaganellioliveira.com.br</span>
						</a>
						<br>
						<a href="<?php echo home_url('agendamento'); ?>">
							<span class="wh">Agende sua consulta <em>aqui</em>.</span>
						</a>
					</div>
					<p class="creditos">ZAGANELLI OLIVEIRA MEDICINA HUMANIZADA © <?php echo date('Y'); ?> Todos os direitos reservados.<br> O conteúdo deste site foi elaborado pela Zaganelli Oliveira Medicina Humanizada e as informações aqui contidas têm caráter meramente informativo e educacional. Não deve ser utilizado para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico, somente ele está habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina. Todas imagens contidas no site são meramente ilustrativas e foram compradas em banco de imagens, não envolvendo imagens de pacientes.<br> Diretora Técnica: Dra. Fabrícia Leal Zaganelli  •  CRM-BA 23345 • RQE 10313</p>
				</div>
			</div>
		</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
