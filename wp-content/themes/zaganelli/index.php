<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zaganelli_Oliveira
 */

get_header('home'); ?>

	<section class="agendamentoHome">
		<div class="container">
			<div class="row full">
				<div class="col-12 col-md-4 offset-md-1 align-self-center text-right">
					<h2 class="agende">Agende <strong>sua consulta</strong> <a href="<?php echo home_url('agendamento'); ?>"><em>aqui</em></a></h2>
					<p>Queremos cuidar de você. Clique no botão abaixo, e você será redirecionado para o nosso pré-agendamento, e, em breve, nossa equipe entrará em contato para confirmar o seu agendamento.</p>

					<a target="_blank" href="https://app.clinicanasnuvens.com.br/agendaExterna?token=5a045a15243fb780ad68496f03d5d575">
						<button class="btAgenda">Agendar aqui</button>
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="especialidades">
		<div class="container-fluid">
			<div class="row">
				<a href="<?php echo home_url('microcirurgia'); ?>" class="item item1 col-12 col-md-9">
					<div>
						<div class="content">
							<h2>Cirurgia de mão</h2>
							<button></button>
						</div>
					</div>
				</a>
			</div>
			<div class="row justify-content-end">
				<a href="<?php echo home_url('fertilizacao-in-vitro-fiv'); ?>" class="item item2 col-12 col-md-6">
					<div class="content">
						<h2>FIV</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="<?php echo home_url('endometriose'); ?>" class="item item3 col-12 col-md-7">
					<div class="content">
						<h2>Endometriose</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row justify-content-end">
				<a href="<?php echo home_url('rinite'); ?>" class="item item4 col-12 col-md-7">
					<div class="content">
						<h2>Rinite</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="<?php echo home_url('doenca-de-alzheimer'); ?>" class="item item5 col-12 col-md-6">
					<div class="content">
						<h2>Alzheimer</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="<?php echo home_url('dermatite-atopica'); ?>" class="item item6 col-12 col-md-6 offset-md-5">
					<div class="content">
						<h2>Dermatite<br> atópica</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row">
				<a href="<?php echo home_url('implante-de-silicone-mamario'); ?>" class="item item7 col-12 col-md-7 offset-md-1">
					<div class="content">
						<h2><strong>Implante de</strong> Silicone<br> Mamário</h2>
						<button></button>
					</div>
				</a>
			</div>
			<div class="row justify-content-center">
				<a href="<?php echo home_url(); ?>" class="item item8 col-12 col-md-7">
					<div class="content">
						<h2><strong>Calendário de</strong> vacinas</h2>
						<button></button>
					</div>
				</a>
			</div>
		</div>
	</section>

	<section class="equipeHome">
		<div class="white"></div>
		<div class="container">
			<div class="row full justify-content-center">
				<div class="col-12 col-md-10  align-self-end">
					<h2>Nossa equipe quer<br> cuidar de você!</h2>
					
					<p>Conheça todo nosso time multicisciplinar e a nossa<br> clínica para o atendimento médico que você precisa</p>
					<a href="<?php echo home_url('equipe'); ?>">
						<button>Nossos<br> especialistas</button>
					</a>

					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/equipeMob.jpg">
				</div>
			</div>
		</div>
	</section>

	<section class="agendablog">
		
		<section class="agenda">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-9 bg">

						<div class="h2">
							<h2><strong>Agende</strong> sua<br> consulta <em>aqui</em></h2>
							<p class="mobile">Nós da Zaganelli Oliveira queremos cuidar de você. Preencha seus dados abaixo e a nossa equipe entrará em contato para confirmar o agendamento de sua consulta.</p>
						</div>
						<div class="row justify-content-end">
							<div class="col-12 col-md-5">
								<p class="desktop">Nós da Zaganelli Oliveira queremos cuidar de você. Preencha seus dados abaixo e a nossa equipe entrará em contato para confirmar o agendamento de sua consulta.</p>

								<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/assets/img/bgAgenda2.png">

								<div id="mauticform_wrapper_agendamentositezaganellioliveira" class="mauticform_wrapper">
								    <form class="formulario" autocomplete="false" role="form" method="post" action="https://mkt.zaganellioliveira.com.br/form/submit?formId=1" id="mauticform_agendamentositezaganellioliveira" data-mautic-form="agendamentositezaganellioliveira" enctype="multipart/form-data">
								        <div class="mauticform-error" id="mauticform_agendamentositezaganellioliveira_error"></div>
								        <div class="mauticform-message" id="mauticform_agendamentositezaganellioliveira_message"></div>
								        <div class="mauticform-innerform">

								            
								          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

								            <div id="mauticform_agendamentositezaganellioliveira_nome" class="mauticform-row mauticform-text mauticform-field-1">
								                <input id="mauticform_input_agendamentositezaganellioliveira_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;"></span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_email" class="mauticform-row mauticform-email mauticform-field-2">
								                <input id="mauticform_input_agendamentositezaganellioliveira_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
								                <span class="mauticform-errormsg" style="display: none;"></span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_celular" class="mauticform-row mauticform-text mauticform-field-3">
								                <input id="mauticform_input_agendamentositezaganellioliveira_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;"></span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
								                <button type="submit" name="mauticform[enviar]" id="mauticform_input_agendamentositezaganellioliveira_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
								            </div>
								            </div>
								        </div>

								        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentositezaganellioliveira_id" value="1">
								        <input type="hidden" name="mauticform[return]" id="mauticform_agendamentositezaganellioliveira_return" value="">
								        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentositezaganellioliveira_name" value="agendamentositezaganellioliveira">

								        </form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>

		<?php $query = new WP_Query(array( 
			'post_type' => 'post',
			'posts_per_page' => 3,
			'order' => 'DESC'
		)); ?>

		<?php if($query->have_posts()): ?>
		<section class="blog">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-9">
						<h2 class="titulo">Blogposts</h2>
						<div class="row">

							<?php while($query->have_posts()): $query->the_post(); ?>

							<?php $index = $query->current_post + 1; ?>


							<div class="item item<?php echo $index; ?> col-12 col-md-<?php if ($index == 1){echo "5";} elseif ($index == 2) {echo "4";} elseif ($index == 3){echo "3";} ?>">
								<a href="<?php the_permalink(); ?>">
									<div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
										<div class="border border1"></div>
										<div class="border border2"></div>
									</div>
									<h3><?php the_title(); ?></h3>
									<button></button>
								</a>
							</div>

							<?php endwhile; ?>

						</div>
						<a href="<?php echo home_url('blog'); ?>">
							<button class="bt">Conheça <em>todos os blogposts</em></button>
						</a>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>

	</section>


	<?php $query = new WP_Query(array( 
		'post_type' => 'ebook',
		'posts_per_page' => 3,
		'order' => 'DESC'
	)); ?>

	<?php if($query->have_posts()): ?>
	<section class="ebooksHome">
		<div class="container">
			<div class="row full justify-content-center">
				<div class="col-12 align-self-center">
					<h2 class="titulo">E-books</h2>
					<div class="slider">
						<div class="sliderEbookHome">
							<?php while($query->have_posts()): $query->the_post(); ?>

							<div class="item">
								<div class="row justify-content-center">
									<div class="col-12 col-md-10">
										<div class="row imagem">
											<div class="col-12 col-md-7">
												<a href="<?php the_permalink(); ?>" target="_self">
													<div class="img">
														<?php the_post_thumbnail(); ?>
													</div>
												</a>
											</div>
										</div>
										<div class="row justify-content-end detalhes">
											<div class="col-12 col-md-6"><a href="<?php the_permalink(); ?>" target="_self">
													<h2><?php the_title(); ?></h2>
													<?php the_field('descricao_curta'); ?>
													<button>Baixe gratuitamente</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<section class="newsletter">
		<div class="container">
			<div class="row full">
				<div class="col-12 col-md-5 offset-md-1 align-self-center">
					<h2 class="titulo">Newsletter</h2>
					<h3>Receba as novidades da Zaganelli Oliveira em seu e-mail.</h3>

					<div id="mauticform_wrapper_newslettersitezaganellioliveira" class="mauticform_wrapper">
					    <form class="formulario" autocomplete="false" role="form" method="post" action="https://mkt.zaganellioliveira.com.br/form/submit?formId=3" id="mauticform_newslettersitezaganellioliveira" data-mautic-form="newslettersitezaganellioliveira" enctype="multipart/form-data">
					        <div class="mauticform-error" id="mauticform_newslettersitezaganellioliveira_error"></div>
					        <div class="mauticform-message" id="mauticform_newslettersitezaganellioliveira_message"></div>
					        <div class="mauticform-innerform">

					            
					          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

					            <div id="mauticform_newslettersitezaganellioliveira_nome" class="mauticform-row mauticform-text mauticform-field-1">
					                <input id="mauticform_input_newslettersitezaganellioliveira_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;"></span>
					            </div>

					            <div id="mauticform_newslettersitezaganellioliveira_email" class="mauticform-row mauticform-email mauticform-field-2">
					                <input id="mauticform_input_newslettersitezaganellioliveira_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
					                <span class="mauticform-errormsg" style="display: none;"></span>
					            </div>

					            <div id="mauticform_newslettersitezaganellioliveira_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-3">
					                <button type="submit" name="mauticform[enviar]" id="mauticform_input_newslettersitezaganellioliveira_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
					            </div>
					            </div>
					        </div>

					        <input type="hidden" name="mauticform[formId]" id="mauticform_newslettersitezaganellioliveira_id" value="3">
					        <input type="hidden" name="mauticform[return]" id="mauticform_newslettersitezaganellioliveira_return" value="">
					        <input type="hidden" name="mauticform[formName]" id="mauticform_newslettersitezaganellioliveira_name" value="newslettersitezaganellioliveira">

					        </form>
					</div>
				</div>
			</div>
		</div>
	</section>

<script type="text/javascript">
    /** This section is only needed once per page if manually copying **/
    if (typeof MauticSDKLoaded == 'undefined') {
        var MauticSDKLoaded = true;
        var head            = document.getElementsByTagName('head')[0];
        var script          = document.createElement('script');
        script.type         = 'text/javascript';
        script.src          = 'https://mkt.zaganellioliveira.com.br/media/js/mautic-form.js';
        script.onload       = function() {
            MauticSDK.onLoad();
        };
        head.appendChild(script);
        var MauticDomain = 'https://mkt.zaganellioliveira.com.br';
        var MauticLang   = {
            'submittingMessage': "Por favor, aguarde..."
        }
    }else if (typeof MauticSDK != 'undefined') {
        MauticSDK.onLoad();
    }
</script>
<?php get_footer();