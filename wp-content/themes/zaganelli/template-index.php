<?php //Template Name: Index ?>
<?php get_header(); ?>

	<section class="especialidades index">
		<div class="container">
			<div class="row justify-content-center">

				<div class="titulo col-12 col-md-10 col-lg-8">
					<h2><?php the_title(); ?></h2>
				</div>

			</div>
		</div>
		<div class="container-fluid">

			<?php

				$args = array(
				    'post_type' => 'page',
				    'posts_per_page' => -1,
				    'post_parent' => $post->ID,
				    'order' => 'ASC',
				    'orderby' => 'name'
				 );
			 ?>
			<?php $parent = new WP_Query($args); ?>
			<?php if ($parent->have_posts()): ?>
				<div class="row justify-content-center">
				<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

					<?php $index = $parent->current_post + 1; ?>
					<?php $total = $parent->found_posts; ?>
						<a href="<?php the_permalink(); ?>" class="item itemIND col-12 col-md-9" style="background-image: url(<?php the_post_thumbnail_url(); ?>); z-index: <?php echo $total - $index ?>;">
							<div class="content">
								<h2><?php the_title(); ?></h2>
								<button></button>
							</div>
						</a>

				<?php endwhile; ?>
				</div>
			<?php else: ?>
				<?php $paginas = get_field('paginas'); ?>
				<?php $index = 1; ?>
				<?php $total = count($paginas); ?>
				<?php if($paginas): ?>
					<div class="row justify-content-center">
						<?php foreach($paginas as $pagina): ?>

					    		<?php $ID = $pagina->ID; ?>

							
								<a href="<?php the_permalink($ID); ?>" class="item itemIND col-12 col-md-9" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID); ?>); z-index: <?php echo $total - $index ?>;">
									<div class="content">
										<h2><?php echo $pagina->post_title; ?></h2>
										<button></button>
									</div>
								</a>

							<?php $index++; ?>

					    <?php endforeach; ?>
					    <?php wp_reset_postdata(); ?>
					</div>
				<?php else: ?>
					<div class="container">
						<div class="row justify-content-center">

							<div class="titulo col-12 col-md-10 col-lg-8">
								<h3>Nenhum conteúdo.</h3>
								<h4>Tente novamente mais tarde.</h4>
							</div>

						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</section>

<?php get_footer();