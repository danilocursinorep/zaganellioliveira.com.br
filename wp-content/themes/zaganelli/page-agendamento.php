<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="agendamento">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-lg-7 offset-lg-4">
						<div class="row cabecalho justify-content-center">
							<div class="col-12 col-lg-10 text-right">
								<h2>Pré agendamento</h2>
							</div>
							<div class="item col-12">
								<p>A Zaganelli Oliveira Medicina Humanizada quer cuidar de você com muita dedicação, experiência, infraestrutura e corpo clínico especializado.</p>

								<p>Clique no botão abaixo, e você será redirecionado para o nosso pré-agendamento, e, em breve, nossa equipe entrará em contato para confirmar o seu agendamento.</p>
								<a target="_blank" href="https://app.clinicanasnuvens.com.br/agendaExterna?token=5a045a15243fb780ad68496f03d5d575">
									<button class="btAgenda">Agendar aqui</button>
								</a>

								<?php /* <div id="mauticform_wrapper_agendamentositezaganellioliveira" class="mauticform_wrapper">
								    <form autocomplete="false" role="form" method="post" action="https://mkt.zaganellioliveira.com.br/form/submit?formId=1" id="mauticform_agendamentositezaganellioliveira" data-mautic-form="agendamentositezaganellioliveira" enctype="multipart/form-data">
								        <div class="mauticform-error" id="mauticform_agendamentositezaganellioliveira_error"></div>
								        <div class="mauticform-message" id="mauticform_agendamentositezaganellioliveira_message"></div>
								        <div class="mauticform-innerform">

								            
								          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

								            <div id="mauticform_agendamentositezaganellioliveira_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
								                <input id="mauticform_input_agendamentositezaganellioliveira_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
								                <input id="mauticform_input_agendamentositezaganellioliveira_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
								                <input id="mauticform_input_agendamentositezaganellioliveira_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_agendamentositezaganellioliveira_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
								                <button type="submit" name="mauticform[enviar]" id="mauticform_input_agendamentositezaganellioliveira_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
								            </div>
								            </div>
								        </div>

								        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentositezaganellioliveira_id" value="1">
								        <input type="hidden" name="mauticform[return]" id="mauticform_agendamentositezaganellioliveira_return" value="">
								        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentositezaganellioliveira_name" value="agendamentositezaganellioliveira">

								        </form>
								</div> */ ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

	<?php /* <script type="text/javascript">
	    if (typeof MauticSDKLoaded == 'undefined') {
	        var MauticSDKLoaded = true;
	        var head            = document.getElementsByTagName('head')[0];
	        var script          = document.createElement('script');
	        script.type         = 'text/javascript';
	        script.src          = 'https://mkt.zaganellioliveira.com.br/media/js/mautic-form.js';
	        script.onload       = function() {
	            MauticSDK.onLoad();
	        };
	        head.appendChild(script);
	        var MauticDomain = 'https://mkt.zaganellioliveira.com.br';
	        var MauticLang   = {
	            'submittingMessage': "Please wait..."
	        }
	    }else if (typeof MauticSDK != 'undefined') {
	        MauticSDK.onLoad();
	    }
	</script> */ ?>


<?php get_footer();