<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="contato">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-lg-8">
						<div class="row cabecalho justify-content-center">
							
							<div class="col-12 col-lg-10">
								<h2>Contato</h2>
							</div>

							<div class="item col-12 col-lg-10">
								<p>Quer mandar uma mensagem para a gente? Deixe-a no campo abaixo ou utilize nossos canais de comunicação.</p><br/>

								<a href="https://goo.gl/maps/w66ai6gH7PSC98VG7">
									<img class="icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/iconePin.png">
									<span>Teixeira de Freitas-BA  •  Cep 45990-289<br/> Av. Getúlio Vargas 2879  • Bela vista</span>
								</a>
								<br/>

								<a href="tel:557332921717">
									<img class="icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeTelefone.png">
									<span>Tel: (73) 32921717</span>
								</a>
								<br/>
								
								<a href="https://wa.me/5573998194979">
									<img class="icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeWhatsapp.png">
									<span>Whatsapp: (73) 99819- 4979</span>
								</a>
								<br/>

								<a href="mailto:recepcao@zaganellioliveira.com.br">
									<img class="icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeMail.png">
									<span>recepcao@zaganellioliveira.com.br</span>
								</a>

								<div id="mauticform_wrapper_contatositezaganellioliveira" class="mauticform_wrapper">
								    <form autocomplete="false" role="form" method="post" action="https://mkt.zaganellioliveira.com.br/form/submit?formId=4" id="mauticform_contatositezaganellioliveira" data-mautic-form="contatositezaganellioliveira" enctype="multipart/form-data">
								        <div class="mauticform-error" id="mauticform_contatositezaganellioliveira_error"></div>
								        <div class="mauticform-message" id="mauticform_contatositezaganellioliveira_message"></div>
								        <div class="mauticform-innerform">

								            
								          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

								            <div id="mauticform_contatositezaganellioliveira_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
								                <input id="mauticform_input_contatositezaganellioliveira_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_contatositezaganellioliveira_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-2 mauticform-required">
								                <input id="mauticform_input_contatositezaganellioliveira_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_contatositezaganellioliveira_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-3 mauticform-required">
								                <input id="mauticform_input_contatositezaganellioliveira_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_contatositezaganellioliveira_mensagem" data-validate="mensagem" data-validation-type="textarea" class="mauticform-row mauticform-text mauticform-field-4 mauticform-required">
								                <textarea id="mauticform_input_contatositezaganellioliveira_mensagem" name="mauticform[mensagem]" placeholder="Deixe sua mensagem" class="mauticform-textarea"></textarea>
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_contatositezaganellioliveira_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
								                <button type="submit" name="mauticform[enviar]" id="mauticform_input_contatositezaganellioliveira_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
								            </div>
								            </div>
								        </div>

								        <input type="hidden" name="mauticform[formId]" id="mauticform_contatositezaganellioliveira_id" value="4">
								        <input type="hidden" name="mauticform[return]" id="mauticform_contatositezaganellioliveira_return" value="">
								        <input type="hidden" name="mauticform[formName]" id="mauticform_contatositezaganellioliveira_name" value="contatositezaganellioliveira">

								        </form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

	<script type="text/javascript">
	    /** This section is only needed once per page if manually copying **/
	    if (typeof MauticSDKLoaded == 'undefined') {
	        var MauticSDKLoaded = true;
	        var head            = document.getElementsByTagName('head')[0];
	        var script          = document.createElement('script');
	        script.type         = 'text/javascript';
	        script.src          = 'https://mkt.zaganellioliveira.com.br/media/js/mautic-form.js';
	        script.onload       = function() {
	            MauticSDK.onLoad();
	        };
	        head.appendChild(script);
	        var MauticDomain = 'https://mkt.zaganellioliveira.com.br';
	        var MauticLang   = {
	            'submittingMessage': "Please wait..."
	        }
	    }else if (typeof MauticSDK != 'undefined') {
	        MauticSDK.onLoad();
	    }
	</script>

<?php get_footer();