<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zaganelli_Oliveira
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x1x80" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<?php global $post; ?>
<?php $slug = $post->post_name; ?>
<body <?php body_class($slug); ?>>

<?php wp_body_open(); ?>
<div id="page" class="home">

	<header id="masthead" class="home-header">
		<div class="container">
			<div class="row justify-content-center">

				<nav id="site-navigation" class="main-navigation col-12 col-lg-4 order-3 order-lg-1 align-self-center align-self-lg-start">
                        	<div id="menu-icone">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
					<div class="menu-container">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'menu-topo',
								'menu_id' => 'primary-menu'
							)
						); ?>
					</div>
				</nav><!-- #site-navigation -->

				<div class="site-branding col-6 col-lg-4 order-1 order-lg-2">
					<a href="<?php echo home_url(); ?>" target="_self">
						<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
					</a>
				</div><!-- .site-branding -->

				<div class="search col-6 col-lg-4 order-2 order-lg-3 align-self-start text-right">
					<img id="search-icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/lupa.png" />
					<?php get_search_form(); ?>
				</div>
				
				<div class="col-12 order-4">
					
					<div class="contentTop row justify-content-center">
						<blockquote class="col-12">
							<h2>Múltiplas especialidades <strong>Múltiplos cuidados</strong></h2>
						</blockquote>
					</div>
					
					<div class="flor row justify-content-center">
						<div class="col-m12 col-md-10">
							
							<div class="row">
								<div class="col-12">
									<a href="<?php echo home_url('alergia-e-imunologia'); ?>">
										<img alt="Imunologia | Adulto e infantil" title="Imunologia | Adulto e infantil" class="parte1 partes circulo imunologia" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/imunologia.png">
									</a>
									<a href="<?php echo home_url('#'); ?>">
										<img alt="Vacinação" title="Vacinação" class="parte2 partes circulo vacinacao" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/vacinacao.png">
									</a>
									<a href="<?php echo home_url('alergia-e-imunologia'); ?>">
										<img alt="Alergia | Adulto e infantil" title="Alergia | Adulto e infantil" class="parte3 partes circulo alergia" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/alergia.png">
									</a>
									<a href="<?php echo home_url('cirurgia-plastica'); ?>">
										<img alt="Cirurgia plástica" title="Cirurgia plástica" class="parte4 partes lateraltop cirurgia" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/cirurgia.png">
									</a>
									<a href="<?php echo home_url('geriatria'); ?>">
										<img alt="Geriatria" title="Geriatria" class="parte5 partes lateraltop geriatria" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/geriatria.png">
									</a>
									<a href="<?php echo home_url('#'); ?>">
										<img alt="Ginecologia" title="Ginecologia" class="parte6 partes lateralbot ginecologia" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/ginecologia.png">
									</a>
									<a href="<?php echo home_url('microcirurgia'); ?>">
										<img alt="Ortopedia: cirurgia da mão" title="Ortopedia: cirurgia da mão" class="parte7 partes centro ortopedia" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/ortopedia.png">
									</a>
									<a href="<?php echo home_url('reproducao-assistida'); ?>">
										<img alt="Reprodução humana" title="Reprodução humana" class="parte8 partes lateralbot reproducao" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/reproducao.png">
									</a>
									<img class="asas" src="<?php echo get_template_directory_uri(); ?>/assets/img/flor/asas.png">
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</header><!-- #masthead -->
	<main id="main" class="site-main">
