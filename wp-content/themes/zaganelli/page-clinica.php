<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="aclinica">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="row cabecalho justify-content-center">
							<div class="col-12 text-lg-right align-self-center">
								<h2>Zaganelli Oliveira</h2>
								<h3>Múltiplas especialidades. <strong>Múltiplos cuidados.</strong></h3>
							</div>
						</div>
					</div>
					<div class="col-12 ">
						<div class="row miviva justify-content-center">
							<div class="item missao col-12 col-md-8">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/missao.png">
								<span>Oferecer atendimento médico de excelência, humanizado e eficiente.</span>
							</div>
							<div class="item visao col-12 col-md-8">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/visao.png">
								<span>Ser referência em Medicina de Excelência no Extremo Sul da Bahia, Norte do Espírito Santo e Nordeste de Minas Gerais.</span>
							</div>
							<div class="item valores col-12 col-md-8">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/valores.png">
								<span>Atendimento personalizado, atencioso, com empatia e calor humano, de forma prática e resolutiva.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();