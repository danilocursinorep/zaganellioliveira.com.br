<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Zaganelli_Oliveira
 */

get_header();
?>

	<section class="pesquisa">
		<div class="container">
			<div class="row cabecalho justify-content-center">
				<div class="col-12 col-12 col-md-10 col-lg-8">
					<?php if ( have_posts() ) : ?>
						<h2>Resultado de<br> sua pesquisa por<br> <strong>"<?php echo get_search_query(); ?>"</strong></h2>
					<?php else: ?>
						<h2>Nenhum resultado<br/> para <strong>"<?php echo $s; ?>".</strong></h2>
						<h3>Tente novamente com uam nova pesquisa.</h3>
					<?php endif; ?>
				</div>
			</div>

			<div class="row justify-content-center">


				<div class="resultado col-12 col-md-10 col-lg-8">
					<ul>
						<?php while (have_posts()): the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<span><strong><?php if (get_page_template_slug(get_the_ID())) {echo 'Institucional';} elseif (get_post_type() == 'page') {echo 'Página';} elseif (get_post_type() == 'ebook') {echo 'E-book';} elseif (get_post_type() == 'post') {echo 'Blogpost';} ?></strong> | <?php the_title(); ?></span>
							</a>
						</li>
						<?php endwhile; ?>
					</ul>
				</div>

				<div class="relacionado col-12 col-md-10 col-lg-6">
					<h3>Confira nossos últimos blogposts:</h3>
					<?php $ID = get_the_ID(); ?>
					<?php $args = array(
						'post__not_in' => array($ID),
						'post_type' => 'post',
						'posts_per_page' => 2, 
					); ?>
					<?php $query = new wp_query( $args ); ?>
					<div class="itens row">
					<?php while ($query->have_posts()): ?>
						<?php $query->the_post();?>
							<div class="item col-12 col-md-6">
							<a href="<?php the_permalink(); ?>">
								<div class="thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
								<h2><?php the_title(); ?></h2>
								<button>Leia mais</button>
							</a>
						</div>

					<?php endwhile ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php /*
	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
					printf( esc_html__( 'Search Results for: %s', 'zaganelli' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header>

			<?php
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main>
	*/ ?>

<?php get_footer();