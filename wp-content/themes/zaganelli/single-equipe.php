<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zaganelli_Oliveira
 */

get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="internaequipe">
			<div class="container">
				<div class="row justify-content-center">

					<div class="titulo col-12 col-md-10 col-lg-8">
						<?php the_category(); ?>
						<h2><?php echo (((strtoupper(substr(get_the_title(),0,3)) == 'DRA'))?'Médica':'Médico'); ?> especialista</h2>
						<a href="<?php echo home_url('equipe'); ?>">
							<button>< Voltar equipe</button>
						</a>
					</div>

					<div class="topo col-12 col-md-10">
						<div class="row">
							<div class="img col-12 col-md-5 align-self-center">
								<div title="<?php the_title(); ?>" class="thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
							</div>
							<div class="det col-12 col-md-7 align-self-center">
								<h2><?php the_title(); ?></h2>
								<h3><?php the_field('cargo'); ?></h3>

								<?php if (get_field('crm')): ?>
								<h4><?php the_field('crm'); ?></h4>
								<?php endif; ?>

								<?php if (get_field('rqe')): ?>
									<h5><?php the_field('rqe'); ?> • <?php the_field('especialidade_rqe'); ?></h5>
								<?php endif; ?>

								<?php if (get_field('rqe2')): ?>
									<h5><?php the_field('rqe2'); ?> • <?php the_field('especialidade_rqe2'); ?></h5>
								<?php endif; ?>
								</a>
							</div>
						</div>
					</div>
					<div class="conteudo col-12 col-md-10 col-lg-8">
						<?php the_field('lattes'); ?>
						<?php if(get_field('url_lattes')): ?>
						<a href="<?php the_field('url_lattes'); ?>" target="_blank">
							<img class="btLattes" src="<?php echo get_template_directory_uri(); ?>/assets/img/btLattes.png">
						<?php endif; ?>
					</div>

				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();