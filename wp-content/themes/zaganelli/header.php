<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Zaganelli_Oliveira
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x1x80" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<?php if ((is_singular()) OR (is_page())) {
	global $post;
	$slug = $post->post_name;
} else {
	$slug = "";
} ?>
<body <?php body_class($slug); ?>>

<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		
	<?php if ((is_page('blog')) OR (is_page('obrigado')) OR (is_page('ebooks')) OR (is_singular('ebook'))): ?>
		<img class="boxshadow blog" src="<?php echo get_template_directory_uri();?>/assets/img/maskBlog.png" />
		<div class="destacada blog" style="background-image: url(<?php
		if (is_singular('ebook')) {
			echo get_template_directory_uri() . '/assets/img/topoEbook.jpg';
		} else {
			the_post_thumbnail_url();
		} ?>);"></div>
	<?php elseif (is_singular('post')): ?>
		<img class="boxshadow post" src="<?php echo get_template_directory_uri();?>/assets/img/maskPost.png" />
		<div class="destacada post" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
	<?php elseif ((is_page('agendamento')) OR (is_404())): ?>
		<img class="boxshadow bola" src="<?php echo get_template_directory_uri();?>/assets/img/maskBola.png" />
		<div class="destacada bola" style="background-image: url(<?php if(is_404()) {echo get_template_directory_uri() . "/assets/img/topo404.jpg";} else {the_post_thumbnail_url();} ?>);"></div>
	<?php else: ?>
		<img class="boxshadow page" src="<?php echo get_template_directory_uri();?>/assets/img/maskPage.png" />
		<div class="destacada page" style="background-image: url(<?php
			if ((is_singular('equipe')) OR (is_archive('equipe'))) {
				echo get_template_directory_uri() . '/assets/img/topoEquipe.jpg';
			} elseif(is_search()) {
				echo get_template_directory_uri() . '/assets/img/topoSearch.jpg';
			} else {
				the_post_thumbnail_url();
			} ?>);"></div>
	<?php endif; ?>

		<div class="container">
			<div class="row justify-content-center">

				<nav id="site-navigation" class="main-navigation col-6 col-lg-4 align-self-center align-self-lg-start">
					<div class="fundo">
						<img class="luckystar" src="<?php echo get_template_directory_uri(); ?>/assets/img/shine.png" />
						<a href="<?php echo home_url(); ?>" target="_self">
							<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
						</a>
	                        	<div id="menu-icone">
	                            <span></span>
	                            <span></span>
	                            <span></span>
	                            <span></span>
	                        </div>
                   		</div>
					<div class="menu-container">
						<?php wp_nav_menu(
							array(
								'theme_location' => 'menu-topo',
								'menu_id' => 'primary-menu'
							)
						); ?>
					</div>
				</nav><!-- #site-navigation -->

				<div class="search col-6 col-lg-4 align-self-start text-right">
					<img id="search-icone" src="<?php echo get_template_directory_uri(); ?>/assets/img/lupa.png" />
					<?php get_search_form(); ?>
				</div>

			</div>

		</div>
	</header><!-- #masthead -->
	<main id="main" class="site-main">
