<?php get_header(); ?>

	

	<section class="blogposts">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10">
					<div class="row cabecalho justify-content-center">
						<div class="col-12 col-md-6 col-lg-4 text-center align-self-center">
							<p>categoria</p>
							<h2><?php echo single_cat_title(); ?></h2>
						</div>
					</div>
					<div class="row itens justify-content-center">
						<?php while(have_posts()): the_post(); ?>
						<div class="item col-12">
							<div class="row">
								<div class="col-12 col-md-5 align-self-center">
									<a href="<?php the_permalink(); ?>">
										<div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
									</a>
								</div>
								<div class="col-12 col-md-7 align-self-center">
									<?php the_category(); ?>
									<a href="<?php the_permalink(); ?>">
										<h2><?php the_title(); ?></h2>
									</a>
								</div>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();