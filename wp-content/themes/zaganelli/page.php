<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Zaganelli_Oliveira
 */

get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="institucional">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 col-lg-8">
						<?php the_category(); ?>
						<h2 class="titulo"><?php the_title(); ?></h2>
						<h3 class="autor">Por: <?php the_author(); ?></h3>
						<div class="content">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="share col-12 col-md-10 col-lg-8">
						<h4>Gostou desse blogpost? <strong>Compartilhe :-)</strong></h4>
						<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
							<img class="icones" src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png">
						</a>
						<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
							<img class="icones" src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png">
						</a>
						<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
							<img class="icones" src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin.png">
						</a>
						<a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
							<img class="icones" src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsapp.png">
						</a>
					</div>
					<div class="comments col-12 col-md-10 col-lg-8">
						<?php if (comments_open() || get_comments_number()): ?>
							<?php comments_template(); ?>
						<?php endif; ?>
					</div>
					<div class="relacionado col-12 col-md-10 col-lg-6">
						<h3>Confira nossos últimos blogposts:</h3>
						<?php $args=array(
								'post_type' => 'post',
								'posts_per_page' => 2, 
							);
						?>
						<?php $my_query = new wp_query( $args ); ?>
						<div class="itens row">
						<?php while ($my_query->have_posts()): ?>
							<?php $my_query->the_post();?>
							<div class="item col-12 col-md-6">
								<a href="<?php the_permalink(); ?>">
									<div class="thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
									<h2><?php the_title(); ?></h2>
									<button>Leia mais</button>
								</a>
							</div>

						<?php endwhile ?>
					</div>

				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();