<?php get_header(); ?>

	<section class="equipe">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10">
					<div class="row cabecalho justify-content-center">
						<div class="col-12 col-md-6 col-lg-7 text-lg-right align-self-center">
							<h2>Nossa Equipe</h2>
						</div>
						<div class="col-12 col-md-6 col-lg-5 align-self-center">
							<p>Conheça os profissionais<br/> que compõe a equipe<br/> multidisciplinar da<br/> Zaganelli Oliveira</p>
						</div>
					</div>
					<div class="row itens justify-content-center">

					<?php while (have_posts()): the_post(); ?>

						<div class="item col-12">
							<div class="row">
								<div class="img col-12 col-md-5 align-self-center">
									<a href="<?php the_permalink(); ?>">
										<div title="<?php the_title(); ?>" class="thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
									</a>
								</div>
								<div class="det col-12 col-md-7 align-self-center">
									<a href="<?php the_permalink(); ?>">
										<h2><?php the_title(); ?></h2>
										<h3><?php the_field('cargo'); ?></h3>
										
										<?php if (get_field('crm')): ?>

										<h4><?php the_field('crm'); ?><?php
										if (get_field('rqe')) {
											echo " •  " . get_field('rqe');
									 	}
										if (get_field('rqe2')) {
											echo " •  " . get_field('rqe2');
									 	}
										?></h4>

										<?php endif; ?>
										<button>Conheça mais da formação <?php echo (((strtoupper(substr(get_the_title(),0,3)) == 'DRA'))?'da':'do'); ?> <?php the_title(); ?></button>
									</a>
								</div>
							</div>
						</div>

					<?php endwhile; ?>

					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();