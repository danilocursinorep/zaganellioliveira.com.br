<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
     <input type="text" class="field" name="s" id="s" placeholder="Buscar" />
<?php if (is_404()): ?>
     <input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/img/lupaSearch.png" value="Pesquisar" id="lupa-icone" />
<?php endif; ?>
</form>