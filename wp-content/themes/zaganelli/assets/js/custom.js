"use strict";

$(document).ready(function () {
  $('#menu-icone').on('click', function () {
    $(this).toggleClass('open');

    if ($('#site-navigation .menu-container').hasClass('show')) {
      $('#site-navigation .menu-container').removeClass('show');
      setTimeout(function () {
        $('#site-navigation .menu-container').removeClass('open');
      }, 400);
    } else {
      $('#site-navigation .menu-container').addClass('open');
      setTimeout(function () {
        $('#site-navigation .menu-container').addClass('show');
      }, 1);
    }
  });
  $('#search-icone').on('click', function () {
    $("#searchform").toggleClass('open');
    $("#searchform .field").focus();
  });
  $('li.menu-item-has-children > a').on('click', function (e) {
    e.preventDefault();
    $(this).parent().parent('ul').toggleClass('hidden');
    $(this).parent().toggleClass('show');

    if ($(this).hasClass('lastMenu')) {
      $(this).removeClass('lastMenu');
      $(this).parent('li').parent('.sub-menu').parent('li').children('a').addClass('lastMenu');
    } else {
      $('.lastMenu').removeClass('lastMenu');
      $(this).addClass('lastMenu');
    }

    $(this).next('.sub-menu').toggleClass('show');
  });
  $('.scrollTop').on('click', function () {
    $("html, body").animate({
      scrollTop: "0"
    });
  });
  var sliderEbooks = $('.slider .sliderEbookHome').slick({
    // prevArrow: $('section .slider .prev'),
    // nextArrow: $('section .slider .next'),
    arrows: true,
    dots: false,
    infinite: true,
    slidesToShow: 1,
    adaptiveHeight: true
  });
  $('.flor .partes').mouseover(function () {
    $('.flor .partes').addClass('hide');
    $('.flor .asas').addClass('big');
    $(this).addClass('show');
  }).mouseout(function () {
    $('.flor .asas').removeClass('big');
    $('.flor .partes').removeClass('hide show');
  });
  $('.celular-ebook, .celular').mask('(00) 00000-0000');
  $(".mauticform-checkboxgrp-checkbox").prop("checked", true);
  $(".preagendar select").change(function () {
    var valor = $(this).val();

    if (valor == "Sim") {
      $(".preagendar_periodo").addClass('show mautic_required');
      $(".preagendar_dia").addClass('show mautic_required');
    } else {
      $(".preagendar_periodo").removeClass('show mautic_required');
      $(".preagendar_dia").removeClass('show mautic_required');
    }
  });
  $(".preagendar select, .preagendar_periodo select, .preagendar_dia select").change(function () {
    var valor = $(this).val();

    if (valor == "") {
      $(this).removeClass('selected');
    } else {
      $(this).addClass('selected');
    }
  });
});