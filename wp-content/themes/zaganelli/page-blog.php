<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="blogposts">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-lg-10">
						<div class="row cabecalho justify-content-center">
							<div class="col-12 col-md-6 col-lg-4 text-lg-right align-self-center">
								<h2>Blog</h2>
							</div>
							<div class="col-12 col-md-6 col-lg-8 align-self-center">
								<p>Artigos para você ficar por dentro de várias<br/> especialidades da Zaganelli Oliveira.</p>
							</div>
						</div>
						<div class="row itens justify-content-center">
							<?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
							<?php $query = new WP_Query(array( 
								'post_type' => 'post',
								'posts_per_page' => 4,
								'order' => 'DESC',
								'paged' => $paged 
							)); ?>
							<?php if($query->have_posts()): ?>
								<?php while($query->have_posts()): $query->the_post(); ?>
								<div class="item col-12">
									<div class="row">
										<div class="col-12 col-md-5 align-self-center">
											<a href="<?php the_permalink(); ?>">
												<div class="img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
											</a>
										</div>
										<div class="col-12 col-md-7 align-self-center">
											<?php the_category(); ?>
											<a href="<?php the_permalink(); ?>">
												<h2><?php the_title(); ?></h2>
											</a>
										</div>
									</div>
								</div>
								<?php endwhile; ?>
								<div class="paginacao text-center col-12 col-lg-8">
									<?php pagination($query); ?>
								</div>
							<?php else: ?>
								<div class="none col-12">
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();