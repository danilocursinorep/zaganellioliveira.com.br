<?php get_header(); ?>

	<section class="notfound">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-lg-7 offset-lg-4">
					<div class="row cabecalho justify-content-center">
						<div class="col-12 col-lg-10">
							<h2>A página não<br/> foi encontrada</h2>
							<p>Não encontramos o tema ou página que você procurou.</p>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="form col-12">
							<p>Gostaria de tentar novamente sua procura?</p>
							<?php get_search_form(); ?>
						</div>
						<div class="outros col-12">
							<h3>Achamos alguns conteúdos do<br> site que você pode se interessar:</h3>

							<?php $args = array(
								's' => $s,
								'post_type' => array('page', 'ebook', 'post'),
								'orderby' => 'rand',
								'posts_per_page' => 10,
							); ?>
							<?php $my_query = new wp_query( $args ); ?>
							<ul>
								<?php while ($my_query->have_posts()): ?>
								<?php $my_query->the_post();?>
								<li>
									<a href="<?php the_permalink(); ?>">
										<span><strong><?php if (get_page_template_slug(get_the_ID())) {echo 'Institucional';} elseif (get_post_type() == 'page') {echo 'Página';} elseif (get_post_type() == 'ebook') {echo 'E-book';} elseif (get_post_type() == 'post') {echo 'Blogpost';} ?></strong> | <?php the_title(); ?></span>
									</a>
								</li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php get_footer();