<?php get_header(); ?>

	<?php while (have_posts()): ?>
		<?php the_post(); ?>

		<section class="internaebook">
			<div class="container">
				<div class="row justify-content-center">

					<div class="topo col-12 col-lg-10">
						<div class="row">
							<div class="col-12 col-md-6">
								<?php the_post_thumbnail(); ?>
							</div>
							<div class="col-12">
								<h2>E-books</h2>

								<?php $categorias = get_field('categoria'); ?>
								<?php if($categorias): ?>
									<ul>
									<?php foreach ($categorias as $categoria): ?>
										<li><?php echo $categoria->name; ?></li>
									<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="conteudo col-12 col-lg-10">
						<div class="row">
							<div class="titulos col-12">
								<h2><?php the_title(); ?></h2>
								<?php the_field('descricao_completa'); ?>
							</div>
							<div class="descricao col-12 col-md-6">
								<?php the_field('descricao'); ?>
							</div>
							<div class="form col-12 col-md-6">
								<?php the_field('formulario_full'); ?>
								<?php the_field('formulario_script'); ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php get_footer();